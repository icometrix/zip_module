// gulp
var gulp = require('gulp');

// plugins
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer')
var browserify = require('browserify');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
var source = require('vinyl-source-stream');
var ngAnnotate = require('gulp-ng-annotate');
var pump = require('pump');

//If yo want to see what browserify-shim is doing
// process.env.BROWSERIFYSHIM_DIAGNOSTICS=1;

var distPath = './dist';

// var zipJsFiles = [
//     './src/js/libjs/zip.js',
//     './src/js/libjs/z-worker.js',
//     './src/js/libjs/deflate.js',
//     './src/js/libjs/inflate.js'
// ];

// clean dist
gulp.task('clean', function (cb) {
  return gulp.src('./dist', {read: false})
      .pipe(clean());
});


gulp.task('build-js', function(cb) {
    var b = browserify('./src/js/app.js');

    //Pump gives better errors
    pump([
        b.bundle(),
        source('zip.min.js'),
        buffer(),
        ngAnnotate(),
        // uglify({outSourceMap: true, mangle : false}),
        gulp.dest(distPath)
    ],
    cb);
});

// runs jshint
gulp.task('jshint', function() {
    gulp.src('./src/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});


gulp.task('build', [ 'jshint', 'build-js'], function() {
});
