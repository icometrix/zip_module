var fs = require('fs');

var obj = require('./../../bower_components/zip-js/WebContent/zip.js');
require('angular');

function createUrl(src){
    var blob = new Blob([src], { type: 'application/javascript' });
    return URL.createObjectURL(blob);
}

var bowerZipJsPath = "";

var zWorker = createUrl(fs.readFileSync(__dirname + '/../../bower_components/zip-js/WebContent/z-worker.js', 'utf8'));
obj.zip.workerScripts = {
    deflater: [zWorker, createUrl(fs.readFileSync(__dirname + '/../../bower_components/zip-js/WebContent/deflate.js', 'utf8'))],
    inflater: [zWorker, createUrl(fs.readFileSync(__dirname + '/../../bower_components/zip-js/WebContent/inflate.js', 'utf8'))]
};

angular.module('zip', [])
    .factory('zip', function(){
        return obj.zip;
    });
