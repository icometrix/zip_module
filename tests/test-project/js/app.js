angular.module('testApp', ['zip'])
  .controller('MainController', ['$scope', 'zip', function($scope, zip) {
          $scope.files = [];
          $scope.logFile = function(){

              zip.createReader(new zip.BlobReader($scope.fileUpload),
                  function (zip_reader) {
                      zip_reader.getEntries(function (entries) {
                          console.log(entries.length + " files found!");
                          for(var i = 0; i < entries.length; i++){
                              console.log(entries[i].filename);
                          }
                      });
                  },
                  //on fail
                  function(message){
                      console.log(message)
                  }
              )
        };

    }]).directive("fileread", [function () {
      return {
          scope: {
              fileread: "="
          },
          link: function (scope, element, attributes) {
              element.bind("change", function (changeEvent) {
                  var reader = new FileReader();
                  reader.onload = function (loadEvent) {
                      scope.$apply(function () {
                          scope.fileread = new Blob([loadEvent.target.result], {type: "octet/stream"});
                      });
                  }
                  reader.readAsArrayBuffer(changeEvent.target.files[0]);
              });
          }
      }
  }]);;
